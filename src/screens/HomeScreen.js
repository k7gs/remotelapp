import React, { useEffect } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, } from "react-native";
import { SafeAreaView } from 'react-native-safe-area-context';

import AsyncStorage from "@react-native-async-storage/async-storage";

import { Surface } from 'react-native-paper';

import IonIcon from "react-native-vector-icons/Ionicons";
import Feather from "react-native-vector-icons/Feather";

const HomeScreen = ({ navigation }) => {
    const _logout = () => {
        AsyncStorage.clear()
        navigation.replace("LoginScreen")
    }

    return (
        <SafeAreaView style={{ flex: 1, paddingTop: 48, paddingHorizontal: 16, backgroundColor: '#f9fcff' }}>
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between' }}>
                <TouchableOpacity
                    onPress={() => navigation.navigate("VitalScreen")}
                    style={styles.tile}>
                    <View style={{ alignItems: 'center' }}>
                        <Feather name="activity" size={40} style={styles.tileIcon} />
                        <Text style={styles.tileText}>Record Vitals</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate("ReminderScreen", { name: 'Reminders' })}
                    style={styles.tile}>
                    <View style={{ alignItems: 'center', }}>
                        <Feather name="clock" size={40} style={styles.tileIcon} />
                        <Text style={styles.tileText}>Med Reminders</Text>
                    </View>
                </TouchableOpacity>
                <View style={styles.tile}>
                    <View style={{ alignItems: 'center' }}>
                        <Feather name="plus" size={40} style={styles.tileIcon} />
                        <Text style={styles.tileText}>Healthcare</Text>
                    </View>
                </View>
                <TouchableOpacity
                    onPress={() => _logout()}
                    style={styles.tile}>
                    <View style={{ alignItems: 'center' }}>
                        <Feather name="user" size={40} style={styles.tileIcon} />
                        <Text style={styles.tileText}>My Profile</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => _logout()}
                    style={styles.tile}>
                    <View style={{ alignItems: 'center' }}>
                        <Feather name="video" size={40} style={styles.tileIcon} />
                        <Text style={styles.tileText}>Video Call</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => _logout()}
                    style={styles.tile}>
                    <View style={{ alignItems: 'center' }}>
                        <Feather name="calendar" size={40} style={styles.tileIcon} />
                        <Text style={styles.tileText}>Appointments</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    tile: {
        marginBottom: 10,
        elevation: 4,
        backgroundColor: 'white',
        width: '49%',
        minHeight: 120,
        alignItems: 'center',
        justifyContent: 'center',
    },
    tileIcon: {
        color: '#0d47a1'
    },
    tileText: {
        marginTop: 12,
        fontSize: 18,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        color: '#0d47a1'
        // color: '#424242',
    },
})

export default HomeScreen;