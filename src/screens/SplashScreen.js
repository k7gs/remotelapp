import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, View, Text, TouchableOpacity, } from "react-native";
import { SafeAreaView } from 'react-native-safe-area-context';

import AsyncStorage from "@react-native-async-storage/async-storage";

import { Surface, TextInput, Button, ActivityIndicator, Colors, } from 'react-native-paper';

import IonIcon from "react-native-vector-icons/Ionicons";
import Feather from "react-native-vector-icons/Feather";

const SplashScreen = ({ route, navigation }) => {
    const [animating, setAnimating] = useState("")

    useEffect(() => {
        console.log("Splash Screen")
        // AsyncStorage.clear();
        AsyncStorage.getItem("token").then((value) => {
            navigation.replace(value === null ? "LoginScreen" : "HomeScreen")
        })
    }, [])

    return (
        <SafeAreaView style={{ flex: 1, paddingHorizontal: 16, backgroundColor: '#f9fcff' }}>
            <ActivityIndicator animating={true} size={48} color={Colors.grey500} style={{ marginTop: 300, }} />
        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    tile: {
        marginBottom: 10,
        elevation: 4,
        backgroundColor: 'white',
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 24,
        borderWidth: 1,
        borderColor: '#f7f7f7',
        borderRadius: 8,
    },
    tileIcon: {
        color: '#0d47a1',
        marginRight: 24,
    },
    formLabel: {
        fontSize: 16,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        // color: '#424242'
        color: '#0d47a1'
        // color: '#424242',
    },
    inputStyle: {
        width: '70%',
        textAlign: 'center',
        backgroundColor: 'transparent',
        fontSize: 20,
        fontWeight: 'bold',
        color: '#444',
    },
    btn: {
        marginTop: 24,
        fontWeight: 'bold',
    },
})

export default SplashScreen;