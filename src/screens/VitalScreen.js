import React, { useEffect } from 'react';
import { ScrollView, StyleSheet, View, Text, TouchableOpacity, } from "react-native";
import { SafeAreaView } from 'react-native-safe-area-context';

import { Surface } from 'react-native-paper';

import IonIcon from "react-native-vector-icons/Ionicons";
import Feather from "react-native-vector-icons/Feather";

const VitalScreen = ({ navigation }) => {
    return (
        <SafeAreaView style={{ flex: 1, paddingHorizontal: 16, backgroundColor: '#f9fcff' }}>
            <ScrollView style={{ flex: 1, paddingVertical: 24, paddingBottom: 60, }}>
                <TouchableOpacity
                    onPress={() => navigation.navigate("WeightRecordScreen", { name: 'Weight', })}
                    style={styles.tile}>
                    <Feather name="plus" size={24} style={styles.tileIcon} />
                    <Text style={styles.tileText}>Weight</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate("Spo2RecordScreen", { name: 'SPO2', })}
                    style={styles.tile}>
                    <Feather name="plus" size={24} style={styles.tileIcon} />
                    <Text style={styles.tileText}>SPO2</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate("PulseRecordScreen", { name: 'Pulse', })}
                    style={styles.tile}>
                    <Feather name="plus" size={24} style={styles.tileIcon} />
                    <Text style={styles.tileText}>Pulse</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate("BpRecordScreen", { name: 'Blood Pressure / BP', })}
                    style={styles.tile}>
                    <Feather name="plus" size={24} style={styles.tileIcon} />
                    <Text style={styles.tileText}>Blood Pressure / BP</Text>
                </TouchableOpacity>
                <View style={{ height: 48 }}></View>
            </ScrollView>
        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    tile: {
        marginBottom: 10,
        elevation: 4,
        backgroundColor: 'white',
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 24,
        borderWidth: 1,
        borderColor: '#f7f7f7',
        borderRadius: 8,
    },
    tileIcon: {
        color: '#0d47a1',
        marginRight: 24,
    },
    tileText: {
        fontSize: 18,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        color: '#0d47a1'
        // color: '#424242',
    },
})

export default VitalScreen;