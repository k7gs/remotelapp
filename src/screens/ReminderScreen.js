import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, View, Text, TouchableOpacity, FlatList } from "react-native";
import { SafeAreaView } from 'react-native-safe-area-context';

import { Surface, TextInput, Button } from 'react-native-paper';

import IonIcon from "react-native-vector-icons/Ionicons";
import Feather from "react-native-vector-icons/Feather";

import firestore from "@react-native-firebase/firestore";

import DatePicker from 'react-native-date-picker';

const ReminderScreen = ({ route, navigation }) => {
    const detail_name = route.params.name
    const [inputText, setInputText] = useState("");
    const [records, setRecords] = useState([]);

    const [date, setDate] = useState(new Date())
    const [open, setOpen] = useState(false)

    const _addRecord = (inputText) => {
        firestore()
            .collection('reminders')
            .add({
                recorded_value: inputText,
                reminder_date: firestore.Timestamp.fromDate(date),
                created_at: firestore.Timestamp.now(),
            })
            .then(() => {
                console.log('Record added!');
            });
    }

    const openDatePicker = () => {
        setShowDatePicker(true)
    }

    const onCancel = () => {
        // You should close the modal in here
        setShowDatePicker(false)
    }

    const onConfirm = (date) => {
        // You should close the modal in here
        setShowDatePicker(false)

        // The parameter 'date' is a Date object so that you can use any Date prototype method.
        console.log(date.getDate())
    }

    useEffect(() => {
        const subscriber = firestore()
            .collection('reminders')
            .onSnapshot(querySnapshot => {
                const data = []

                querySnapshot.forEach(documentSnapshot => {
                    data.push({
                        ...documentSnapshot.data(),
                        id: documentSnapshot.id,
                    })
                })

                setRecords(data)
            });

        return () => subscriber();
    }, [])

    return (
        <SafeAreaView style={{ flex: 1, paddingHorizontal: 0, backgroundColor: '#f9fcff' }}>
            <ScrollView style={{ flex: 1, paddingVertical: 24, paddingBottom: 150, paddingHorizontal: 16, }}>
                <View style={{ alignItems: 'center', }}>
                    <Text style={styles.formLabel}>Add Reminder</Text>
                    <TextInput
                        style={styles.inputStyle}
                        value={inputText}
                        onChangeText={value => setInputText(value)}
                    />
                    <Button icon="calendar" mode="contained" onPress={() => setOpen(true)} style={styles.btn1}>
                        Set Date & Time
                    </Button>
                    <Text style={{ paddingVertical: 12, fontWeight: 'bold' }}>{date.toLocaleString()}</Text>
                    <DatePicker
                        modal
                        open={open}
                        date={date}
                        onConfirm={(date) => {
                            setOpen(false)
                            setDate(date)
                        }}
                        onCancel={() => {
                            setOpen(false)
                        }}
                    />
                    <Button icon="plus" mode="contained" onPress={() => _addRecord(inputText)} style={styles.btn} dark>
                        Save Reminder
                    </Button>
                </View>
            </ScrollView>
            <View style={{ paddingHorizontal: 16, }}>
                <Text style={{ fontSize: 17, fontWeight: '700', textTransform: 'uppercase' }}>My Reminders</Text>
            </View>
            <FlatList
                data={records}
                renderItem={({ item }) => (
                    <View style={{ flex: 1, borderColor: '#ddd', borderTopWidth: 1, paddingHorizontal: 16, paddingVertical: 8 }}>
                        <Text style={{ fontSize: 17, fontWeight: '700', color: '#0d47a1' }}>{item.recorded_value}</Text>
                        <Text style={{ fontWeight: 'bold', color: '#444' }}>{new Date(item.reminder_date.toDate()).toLocaleString()}</Text>
                    </View>
                )}
            />

        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    tile: {
        marginBottom: 10,
        elevation: 4,
        backgroundColor: 'white',
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 24,
        borderWidth: 1,
        borderColor: '#f7f7f7',
        borderRadius: 8,
    },
    tileIcon: {
        color: '#0d47a1',
        marginRight: 24,
    },
    formLabel: {
        fontSize: 16,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        // color: '#424242'
        color: '#0d47a1'
        // color: '#424242',
    },
    inputStyle: {
        width: '70%',
        textAlign: 'center',
        backgroundColor: 'transparent',
        fontSize: 20,
        fontWeight: 'bold',
        color: '#444',
    },
    btn1: {
        marginTop: 24,
        fontWeight: 'bold',
    },
    btn: {
        marginTop: 24,
        fontWeight: 'bold',
        backgroundColor: 'green',
    },
})

export default ReminderScreen;