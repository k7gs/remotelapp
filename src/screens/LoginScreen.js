import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, View, Text, TouchableOpacity, Image, } from "react-native";
import { SafeAreaView } from 'react-native-safe-area-context';

import AsyncStorage from "@react-native-async-storage/async-storage";

import { Surface, TextInput, Button, ActivityIndicator, Colors } from 'react-native-paper';

import IonIcon from "react-native-vector-icons/Ionicons";
import Feather from "react-native-vector-icons/Feather";

const LoginScreen = ({ route, navigation }) => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [animating, setAnimating] = useState(false);

    const _authenticate = async () => {
        setAnimating(true);
        if (username === 'admin@remotelhealthcare.com' && password === 'admin123') {
            setTimeout(async () => {
                try {
                    await AsyncStorage.setItem("token", username)
                    navigation.replace("HomeScreen")
                } catch (error) {
                    console.log("Some error occured.")
                }
            }, 3000);
        } else {
            setTimeout(() => {
                alert("Wrong credentials.")
                navigation.replace("HomeScreen")
                setAnimating(false);
            }, 3000)
        }
    }

    const _authLayer = () => {
    }

    return (
        <SafeAreaView style={{ flex: 1, paddingHorizontal: 16, backgroundColor: '#ffffff' }}>
            <ScrollView style={{ flex: 1, paddingVertical: 96, paddingBottom: 48, }}>
                <View style={{ alignItems: 'center', }}>
                    <Image
                        style={styles.imgLogo}
                        source={require('../assets/rLogo.png')}
                    />
                    <Text style={styles.logaiName}>Remotel</Text>
                    <Text style={styles.formLabel}>Please login to continue</Text>
                    {animating === true
                        ?
                        <ActivityIndicator animating={animating} size={48} color={Colors.grey500} style={{ marginBottom: 24, }} />
                        :
                        null
                    }
                    <TextInput
                        label="Email"
                        style={styles.inputStyle}
                        value={username}
                        onChangeText={value => setUsername(value)}
                    />
                    <TextInput
                        label="Password"
                        style={styles.inputStyle}
                        value={password}
                        secureTextEntry={true}
                        onChangeText={value => setPassword(value)}
                    />
                    <Button mode="contained" onPress={() => _authenticate()} style={styles.btn} dark>
                        Login to your account
                    </Button>
                </View>
            </ScrollView>
        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    tile: {
        marginBottom: 10,
        elevation: 4,
        backgroundColor: 'white',
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 24,
        borderWidth: 1,
        borderColor: '#f7f7f7',
        borderRadius: 8,
    },
    tileIcon: {
        color: '#0d47a1',
        marginRight: 24,
    },
    formLabel: {
        fontSize: 16,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        color: '#424242',
        marginBottom: 24,
    },
    inputStyle: {
        width: '90%',
        fontSize: 17,
        backgroundColor: '#f9fcff',
        fontWeight: 'bold',
        marginBottom: 24,
        height: 56,
        color: '#444',
    },
    btn: {
        marginTop: 24,
        fontWeight: 'bold',
    },
    imgLogo: {
        marginBottom: 12,
    },
    logaiName: {
        color: '#0d47a1',
        fontSize: 24,
        textTransform: 'uppercase',
        marginBottom: 24,
        letterSpacing: 1,
        fontWeight: 'bold',
    },
})

export default LoginScreen;