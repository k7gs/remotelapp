/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import SplashScreen from './src/screens/SplashScreen';
import LoginScreen from './src/screens/LoginScreen';
import HomeScreen from "./src/screens/HomeScreen";
import VitalScreen from './src/screens/VitalScreen';
import VitalDetailScreen from './src/screens/VitalDetailScreen';
import WeightRecordScreen from './src/screens/WeightRecordScreen';
import Spo2RecordScreen from './src/screens/Spo2RecordScreen';
import PulseRecordScreen from './src/screens/PulseRecordScreen';
import BpRecordScreen from './src/screens/BpRecordScreen';
import ReminderScreen from './src/screens/ReminderScreen';

const Stack = createStackNavigator();

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#0d47a1',
    accent: '#f1c40f',
  },
};

const App = () => {
  return (
    <PaperProvider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="SplashScreen">
          <Stack.Screen name="SplashScreen" component={SplashScreen} options={{ title: 'Remotel Health Care' }} />
          <Stack.Screen name="LoginScreen" component={LoginScreen} options={{ headerShown: false, title: 'Remotel Health Care' }} />
          <Stack.Screen name="HomeScreen" component={HomeScreen} options={{ title: 'Remotel Health Care' }} />
          <Stack.Screen name="VitalScreen" component={VitalScreen} options={{ title: 'Record Vitals' }} />
          <Stack.Screen name="WeightRecordScreen" component={WeightRecordScreen} options={{ title: 'Weight Records' }} />
          <Stack.Screen name="Spo2RecordScreen" component={Spo2RecordScreen} options={{ title: 'Spo2 Records' }} />
          <Stack.Screen name="PulseRecordScreen" component={PulseRecordScreen} options={{ title: 'Pulse Records' }} />
          <Stack.Screen name="BpRecordScreen" component={BpRecordScreen} options={{ title: 'Blood Pressure Records' }} />
          <Stack.Screen name="ReminderScreen" component={ReminderScreen} options={{ title: 'Medical Reminders' }} />
          <Stack.Screen name="VitalDetailScreen" component={VitalDetailScreen} options={{ title: 'Vital Details' }} />
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  )
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
